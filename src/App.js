import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      chartDataTracker: {startIdx: 0, endIdx: 4},
      chartData: {},
      extractedChartData: [],
      chartConfig: {
        title: {
          text: 'Character Counts'
        },
        legend: {
          enabled: false
        },
        xAxis: {
          title: {
            text: 'Characters'
          },
          categories: [
            'A', 'B', 'C', 'D', 'E',
            'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y',
            'Z', 'Other'
          ],
          min: 0,
          max: 26
        },
        yAxis: {
          title: {
            text: 'Count',
          }
        },
        series: [{
          name: 'Count',
          data: null
        }]
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.extractData = this.extractData.bind(this)
    this.handleChart = this.handleChart.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    fetch('https://api.github.com/search/users?q=' + this.state.value)
    .then((response) => response.json())
    .then((data) => {
      if (!data.errors) {
        this.setState({chartDataTracker: {startIdx: 0, endIdx: 4}});
        this.setState({chartData: data});
        this.extractData();
      }
    });
  }

  handleClick(event) {
    event.preventDefault();
    this.extractData();
  }

  extractData() {
    const items = this.state.chartData.items;
    let loginNames = [];
    let loginChars = [];
    let extractedData = {};
    let otherCharCount = 0;
    let extractedDataArr;

    for (let i = this.state.chartDataTracker.startIdx; i <= this.state.chartDataTracker.endIdx; i++) {
      items[i] && loginNames.push(items[i].login.toUpperCase());
    }

    for (let i = 'A'.charCodeAt(0); i <= 'Z'.charCodeAt(0); i++) {
      extractedData[i] = 0;
    }

    loginChars = loginNames.join('').split('').sort();

    loginChars.forEach((char) => {
      let charCode = char.charCodeAt(0);
      charCode < 'A'.charCodeAt(0) || charCode > 'Z'.charCodeAt(0) ? otherCharCount++ : extractedData[charCode]++
    })

    this.setState({chartDataTracker: { startIdx: this.state.chartDataTracker.startIdx + 5, endIdx: this.state.chartDataTracker.endIdx + 5}});
    extractedDataArr = Object.values(extractedData);
    extractedDataArr.push(otherCharCount)
    this.setState({extractedChartData: extractedDataArr}, this.handleChart);
  }

  handleChart() {
    let chart = this.refs.chart.getChart();

    this.setState({chartConfig: {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Character Counts'
      },
      legend: {
        enabled: false
      },
      xAxis: {
        title: {
          text: 'Characters'
        },
        categories: [
          'A', 'B', 'C', 'D', 'E',
          'F', 'G', 'H', 'I', 'J',
          'K', 'L', 'M', 'N', 'O',
          'P', 'Q', 'R', 'S', 'T',
          'U', 'V', 'W', 'X', 'Y',
          'Z', 'Other'
        ],
        crosshair: true,
        min: 0,
        max: 26
      },
      yAxis: {
        title: {
          text: 'Count',
        },
        min: 0
      },
      series: [{
        name: 'Count',
        data: this.state.extractedChartData
      }]
    }});
  }

  render() {
    return (
      <div className="App">
        <br />
        <form onSubmit={this.handleSubmit}>
          <label>
            <input type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
          <button onClick={this.handleClick}>See Next Five Items</button>
        </form>
        <br />
        <ReactHighcharts config={this.state.chartConfig} isPureConfig ref="chart"></ReactHighcharts>
      </div>
    );
  }
}

export default App;
